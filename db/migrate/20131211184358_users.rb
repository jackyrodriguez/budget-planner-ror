class Users < ActiveRecord::Migration
  def up
  	create_table :users do |t|
      t.string :firstname
      t.string :lastname
      t.string :email
      t.string :encrypted_password
      t.string :salt
      t.timestamps
  	end
  end

  def down
  end
end
