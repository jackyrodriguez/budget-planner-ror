class CreateExpenses < ActiveRecord::Migration
  def change
    create_table :expenses do |t|
      t.string :categories
      t.string :amount
      t.text :description
      t.string :email

      t.timestamps
    end
  end
end
