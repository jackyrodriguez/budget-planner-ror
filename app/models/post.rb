class Post < ActiveRecord::Base
  attr_accessible :text, :title
  validates :title, presence: true, length: { minimum: 5 }
  validates :title, :presence => true, :length => { :minimum => 2 }
end
