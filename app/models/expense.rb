class Expense < ActiveRecord::Base
  attr_accessible :amount, :categories, :description, :email
  validates :amount, :presence => true
  #validates :categories, inclusion: ['food','transportation','clothing']
  validates :description, :presence => true, :length => {:minimum => 2}
  EMAIL_REGEX = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i
  validates :email, :presence => true, :format => EMAIL_REGEX

end
